package com.ilabservice.darwinrecorder.hlslive.repository;

import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.Lob;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.stream.Collectors;

public interface EvtVideoRepository extends JpaRepository<EvtVideo,Long>{
    List<EvtVideo> findByVideo(String video);
    List<EvtVideo> findByVideoIn(List<String> videos);
    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(long endTime, long startTime, String cameraId);
    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraIdAndVaEndTimeGreaterThanAndVaCode(long endTime, long startTime, String cameraId,Long vaEndTime,String vaCode);
    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThan(long endTime, long startTime);
    void deleteByCameraIdAndStartTime(String cameraId,long startTime);
    void deleteByCameraId(String cameraId);
    List<EvtVideo> findByCameraId(String cameraId);
    List<EvtVideo> findByImage(String image);
    List<EvtVideo> findByCameraIdAndStartTime(String cameraId,long startTime);
    List<EvtVideo> findByCameraIdAndStartTimeAndVaresultContaining(String cameraId,long startTime,String varesult);
    List<EvtVideo> findByCameraIdAndStartTimeAndEndTimeAndVaresultContaining(String cameraId,long startTime,long endTime,String varesult);



}
