package com.ilabservice.darwinrecorder.hlslive.cron;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import com.ilabservice.darwinrecorder.hlslive.measurements.DarwinMeasurement;
import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DarwinListener {

    @Autowired
    List<String> darwinList;

    @Autowired
    InfluxDbUtils influxDbUtils;

    @Autowired
    RestTemplate restTemplate;

    public List<String> getAllStream(){
        List<String> allStream = new ArrayList<String>();
        darwinList.stream().forEach(s -> {
            String json = restTemplate.getForObject(s, String.class);
            JSONObject jsonObject = JSONObject.parseObject(json);
            int total = jsonObject.getIntValue("total");
            if (total > 0) {
                JSONArray jSONArray = jsonObject.getJSONArray("rows");
                jSONArray.stream().map(jo -> {
                    JSONObject jo1 = (JSONObject) jo;
                    return jo1;
                }).forEach(s1 -> allStream.add(StringUtils.substringAfterLast(s1.getString("source"),"/")));
            }
        });
        Collections.sort(allStream); return allStream;
    }


    public List<String> getAllRTSP(){
        List<String> allStream = new ArrayList<String>();

        darwinList.stream().forEach(s -> {
            String json = restTemplate.getForObject(s, String.class);
            JSONObject jsonObject = JSONObject.parseObject(json);
            int total = jsonObject.getIntValue("total");
            if (total > 0) {
                JSONArray jSONArray = jsonObject.getJSONArray("rows");
                jSONArray.stream().map(jo -> {
                    JSONObject jo1 = (JSONObject) jo;
                    return jo1;
                }).forEach(s1 -> allStream.add(s1.getString("source")));
            }
        });
        Collections.sort(allStream);

        if(allStream.size()>0){
            DarwinMeasurement dm = new DarwinMeasurement();
            dm.setTime(System.currentTimeMillis());
            dm.setTotalStreams(allStream.size());
            dm.setClusterSize(darwinList.size());
            dm.setType("darwin");
            Point point = Point.measurementByPOJO(dm.getClass()).addFieldsFromPOJO(dm).build();
            influxDbUtils.influxDB.write(point);
        }
        return allStream;
    }


}
