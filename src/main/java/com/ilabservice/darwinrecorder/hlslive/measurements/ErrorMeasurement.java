package com.ilabservice.darwinrecorder.hlslive.measurements;

import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;

@Measurement(name= "ERROR_MEASUREMENTS")
public class ErrorMeasurement {
    public String getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = String.valueOf(time);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="time")
    private String time;
    @Column(name="type")
    private String type;
    @Column(name="reason")
    private String reason;
    @Column(name="name")
    private String name;
}
