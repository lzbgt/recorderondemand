package com.ilabservice.darwinrecorder.hlslive.config;

import com.ilabservice.darwinrecorder.hlslive.controller.StreamRecorderController;
import com.ilabservice.darwinrecorder.hlslive.util.MQTTMessageHandler;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.handler.annotation.Header;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MQTTConfiguration {
    @Value("${mqtt.producer.servers}")
    private String servers;
    @Value("${mqtt.producer.client-id}")
    private String clientId;
    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;
    @Value("${mqtt.consumer.default-topic}")
    private String defaultConsumerTopic;
    @Value("${mqtt.producer.default-qos}")
    private int qos;
    @Value("{mqtt.connect.username}")
    private String username;
    @Value("{mqtt.connect.password}")
    private String password;
    @Value("{mqtt.consumer.client-id}")
    private String consumerId;

    @Autowired
    MQTTMessageHandler mqttMessageHandler;

    private static final Logger log = LoggerFactory.getLogger(MQTTConfiguration.class);

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(getSenderMqttConnectOptions());
        return factory;
    }


    @Bean
    public MqttConnectOptions getSenderMqttConnectOptions(){
        MqttConnectOptions options=new MqttConnectOptions();
        if(!username.trim().equals("")){
            options.setUserName(username);
        }
        options.setPassword(password.toCharArray());
        options.setServerURIs(StringUtils.split(servers, ","));
        options.setConnectionTimeout(10);
        options.setKeepAliveInterval(20);
        options.setCleanSession(true); //clear the session to prevent duplication
//        options.setWill("willTopic", WILL_DATA, 2, false);
        return options;
    }


    @Bean
    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler(MqttAsyncClient.generateClientId(), mqttClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultQos(qos);
        messageHandler.setDefaultTopic(defaultTopic);
        return messageHandler;
    }

    @Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    @MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
    public interface MqttGateway {
        void sendToMqtt(String payload);
        void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, String payload);
        void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos, String payload);
    }

//    @Bean
//    public MessageChannel mqttInputChannel() {
//        return new DirectChannel();
//    }

//    @Bean
//    public MessageProducer inbound() {
//        MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter(MqttAsyncClient.generateClientId(),
//                mqttClientFactory(), defaultConsumerTopic);
//        adapter.setCompletionTimeout(5000);
//        adapter.setConverter(new DefaultPahoMessageConverter());
//        adapter.setQos(qos);
//        adapter.setOutputChannel(mqttInputChannel());
//        return adapter;
//    }

//    @Bean
//    @ServiceActivator(inputChannel = "mqttInputChannel")
//    public MessageHandler messageHandler() {
//        return message -> {
////            log.info("message received, start to handle ..." + message.getPayload().toString());
////            String payload = message.getPayload().toString();
////            String topic = message.getHeaders().get("mqtt_receivedTopic").toString();
////            if (topic.equals(defaultConsumerTopic)) {
////                log.info("message received, start to handle ..." + payload + " with topic " + topic);
////                mqttMessageHandler.handlePayload_evt(payload);
////            }
//        };
//    }


    @Bean
    public void handleSharedTopic() throws MqttException {
        MqttClient client = new MqttClient(servers,MqttAsyncClient.generateClientId(),new MemoryPersistence());
        MqttConnectOptions connOpts = getSenderMqttConnectOptions(); //the connect opt
        client.connect(connOpts);
        String sharedTopic = defaultConsumerTopic;
        Map<String, IMqttMessageListener> listeners = new HashMap<>();
        listeners.put(sharedTopic, new IMqttMessageListener() {
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                log.info(" detected topic " + topic );
                log.info("message received, start to handle ..." + message.toString() + " with topic " + topic);
                mqttMessageHandler.handlePayload_evt(message.toString());
            }
        });
        client.setCallback(new SharedSubCallbackRouter(listeners));
        client.subscribe(sharedTopic);
    }


}
